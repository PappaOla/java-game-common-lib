package io.hellgren.game;

/**
 * Quest response class
 */
public class QuestResponse {

  private boolean success;
  private String message;

  /**
   * Response class to return when a quest is tested
   * @param success indicates if the quest was passed or not
   * @param message message to send if test was unsuccessful
   */
  public QuestResponse(boolean success, String message) {
    this.success = success;
    this.message = message;
  }

  public boolean isSuccess() {
    return success;
  }

  public String getMessage() {
    return message;
  }
}
