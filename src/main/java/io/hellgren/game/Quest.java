package io.hellgren.game;

import java.util.List;

/**
 * Quest are task for a player to complete. It could be providing something on a specified path or
 * really anything, only your imagination sets the boundaries.
 */
public interface Quest {

  /**
   * This method will be run to test if a player has completed the quest.
   *
   * A playerId and baseUrl wil be provided so that you can use this to know
   * that a specific player has completed the quest.
   *
   * Example of a quest can be to check a specific url:
   * GET http://10.0.1.123:1234/api/hello
   * should respond with 'Hello!'
   *
   * or
   *
   * Kafka message with playerId should be available on kafka queue 'java.game.test'
   *
   * The execution should be fast.
   *
   * @param playerId id for the player trying to complete the test
   * @param baseUrl base url for player, eg. "http://player1.com"
   * @return QuestResponse containing `true` if quest complete/passed
   */
  QuestResponse test(String playerId, String baseUrl) throws Exception;

  /**
   * A name for the quest
   * @return name of quest
   */
  String getName();

  /**
   * Description on what should be achieved in the quest
   * @return a description
   */
  String getDescription();

  /**
   * Default for quests are that they are visible.
   * That means that the player will be shown description on what to do.
   *
   * If a quest is hidden the description will not be shown and the player
   * won't know that they been assigned the quest.
   * Instead the player has to figure out what is wrong.
   * @return true if hidden
   */
  default boolean isHidden() {
    return false;
  }

  /**
   * List of hints that can help a player complete a quest. These wll be shown after a set time
   * if the player has not yet completed the quest. One hit at a time will be shown.
   * @return a hint
   */
  List<String> getHints();

}
