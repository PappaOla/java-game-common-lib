package io.hellgren.game;

import java.util.List;

/**
 * This class is used to create at game character (NPC, Non-Player Character) that can give out quests to players.
 *
 * This is where you'll start if building your own quest.
 */
public interface Npc {

  /**
   * A name for the character
   * @return a name of a npc
   */
  String getName();

  /**
   * A description or pharse that corresponds to this character
   * @return description or npc specific phrase
   */
  String getDescription();

  /**
   * This should contain a list of instansiated quest classes that the npc will give out tho players.
   *
   * The quests will be given out in the order they appear in the list.
   * @return a list of quests
   */
  List<Quest> getQuestList();

}
